<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 */
class Album
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreAlbum;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Playliste", mappedBy="album")
     */
    private $playlistes;

    /**
      * @ORM\ManyToMany(targetEntity="App\Entity\Music", mappedBy="album")
     */
    private $musics;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Artiste", inversedBy="albums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $artiste;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Music", inversedBy="albums")
     */
    private $music;

    public function __construct()
    {
        $this->playlistes = new ArrayCollection();
        $this->musics = new ArrayCollection();
        $this->music = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreAlbum(): ?string
    {
        return $this->titreAlbum;
    }

    public function setTitreAlbum(string $titreAlbum): self
    {
        $this->titreAlbum = $titreAlbum;

        return $this;
    }

    /**
     * @return Collection|Playliste[]
     */
    public function getPlaylistes(): Collection
    {
        return $this->playlistes;
    }

    public function addPlayliste(Playliste $playliste): self
    {
        if (!$this->playlistes->contains($playliste)) {
            $this->playlistes[] = $playliste;
            $playliste->addAlbum($this);
        }

        return $this;
    }

    public function removePlayliste(Playliste $playliste): self
    {
        if ($this->playlistes->contains($playliste)) {
            $this->playlistes->removeElement($playliste);
            $playliste->removeAlbum($this);
        }

        return $this;
    }

    /**
     * @return Collection|Music[]
     */
    public function getMusics(): Collection
    {
        return $this->musics;
    }

    public function addMusic(Music $music): self
    {
        if (!$this->musics->contains($music)) {
            $this->musics[] = $music;
            $music->addAlbum($this);
        }

        return $this;
    }

    public function removeMusic(Music $music): self
    {
        if ($this->musics->contains($music)) {
            $this->musics->removeElement($music);
            $music->removeAlbum($this);
        }

        return $this;
    }

    public function getArtiste(): ?Artiste
    {
        return $this->artiste;
    }

    public function setArtiste(?Artiste $artiste): self
    {
        $this->artiste = $artiste;

        return $this;
    }

    /**
     * @return Collection|Music[]
     */
    public function getMusic(): Collection
    {
        return $this->music;
    }
}
