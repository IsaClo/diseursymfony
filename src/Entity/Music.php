<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MusicRepository")
 */
class Music
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Playliste", mappedBy="music")
     */
    private $playlistes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artiste", inversedBy="musics")
     */
    private $artiste;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", inversedBy="musics")
     */
    private $album;

    /**
     * @ORM\Column(type="object")
     */
    private $mp3;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artiste", mappedBy="music")
     */
    private $artistes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", mappedBy="music")
     */
    private $albums;

    public function __construct()
    {
        $this->playlistes = new ArrayCollection();
        $this->artiste = new ArrayCollection();
        $this->album = new ArrayCollection();
        $this->artistes = new ArrayCollection();
        $this->albums = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    /**
     * @return Collection|Playliste[]
     */
    public function getPlaylistes(): Collection
    {
        return $this->playlistes;
    }

    public function addPlayliste(Playliste $playliste): self
    {
        if (!$this->playlistes->contains($playliste)) {
            $this->playlistes[] = $playliste;
            $playliste->addMusic($this);
        }

        return $this;
    }

    public function removePlayliste(Playliste $playliste): self
    {
        if ($this->playlistes->contains($playliste)) {
            $this->playlistes->removeElement($playliste);
            $playliste->removeMusic($this);
        }

        return $this;
    }

    /**
     * @return Collection|Artiste[]
     */
    public function getArtiste(): Collection
    {
        return $this->artiste;
    }

    public function addArtiste(Artiste $artiste): self
    {
        if (!$this->artiste->contains($artiste)) {
            $this->artiste[] = $artiste;
        }

        return $this;
    }

    public function removeArtiste(Artiste $artiste): self
    {
        if ($this->artiste->contains($artiste)) {
            $this->artiste->removeElement($artiste);
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbum(): Collection
    {
        return $this->album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->album->contains($album)) {
            $this->album[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->album->contains($album)) {
            $this->album->removeElement($album);
        }

        return $this;
    }

    public function getMp3()
    {
        return $this->mp3;
    }

    public function setMp3($mp3): self
    {
        $this->mp3 = $mp3;

        return $this;
    }

    /**
     * @return Collection|Artiste[]
     */
    public function getArtistes(): Collection
    {
        return $this->artistes;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }
}
