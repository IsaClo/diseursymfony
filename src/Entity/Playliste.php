<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaylisteRepository")
 */
class Playliste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titrePlayliste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="playliste")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Music", inversedBy="playlistes")
     */
    private $music;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", inversedBy="playlistes")
     */
    private $album;

    public function __construct()
    {
        $this->music = new ArrayCollection();
        $this->album = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitrePlayliste(): ?string
    {
        return $this->titrePlayliste;
    }

    public function setTitrePlayliste(string $titrePlayliste): self
    {
        $this->titrePlayliste = $titrePlayliste;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Music[]
     */
    public function getMusic(): Collection
    {
        return $this->music;
    }

    public function addMusic(Music $music): self
    {
        if (!$this->music->contains($music)) {
            $this->music[] = $music;
        }

        return $this;
    }

    public function removeMusic(Music $music): self
    {
        if ($this->music->contains($music)) {
            $this->music->removeElement($music);
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbum(): Collection
    {
        return $this->album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->album->contains($album)) {
            $this->album[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->album->contains($album)) {
            $this->album->removeElement($album);
        }

        return $this;
    }
}
